" General
set nocompatible
filetype plugin indent on
syntax on
set autoindent
set inccommand=split
set title
set mouse=a
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set termguicolors
set hidden
set number
set inccommand=split
set so=99999
set wildmenu
set wildmode=list:longest,full
set undofile
if has('win32')
    set undodir=~/AppData/Local/nvim-data/undo
else
    set undodir=~/.local/share/nvim/undo
end
set undolevels=1000
set undoreload=10000
set foldmethod=syntax
set foldcolumn=5
autocmd BufRead * normal zR
set spelllang=en,de
set spell
nnoremap <Leader>h :noh<CR>
nnoremap <Leader>\c :copen<cr>
nnoremap <Leader>o :%bd\|e#\|bd#<cr>
autocmd BufRead,BufNewFile * setlocal signcolumn=yes
autocmd FileType tagbar,nerdtree setlocal signcolumn=no
set list listchars=tab:>-,trail:·
autocmd Filetype tex autocmd BufWritePre <buffer> %s/\s\+$//e
autocmd BufNewFile,BufRead *.cls set syntax=tex
set clipboard=unnamedplus
let fortran_free_source=1
let fortran_more_precise=1
let fortran_do_enddo=1

" GUI
set guifont=Fira\ Code\:h10

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
"" Autocompletion
Plug 'chrisbra/unicode.vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"" Files
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"" Code
Plug 'scrooloose/nerdcommenter'
Plug 'neomake/neomake'
Plug 'AndrewRadev/switch.vim'
Plug 'sbdchd/neoformat'
Plug 'godlygeek/tabular'
Plug 'dhruvasagar/vim-table-mode'
"" Interface
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'morhetz/gruvbox'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'kshenoy/vim-signature'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'wfxr/minimap.vim'
"" Session Management
Plug 'tpope/vim-obsession'
Plug 'dhruvasagar/vim-prosession'
"" Filetypes
Plug 'vhda/verilog_systemverilog.vim'
Plug 'lervag/vimtex'
Plug 'rust-lang/rust.vim'
Plug 'tikhomirov/vim-glsl'
Plug 'cespare/vim-toml'
Plug 'maralla/vim-toml-enhance'
Plug 'wannesm/wmgraphviz.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'dag/vim-fish'
Plug 'calviken/vim-gdscript3'
Plug 'pest-parser/pest.vim'
call plug#end()

" general
command Upgrade PlugUpgrade | PlugUpdate | UpdateRemotePlugins | CocUpdate

" coc
nnoremap <Leader><Leader>h :call CocActionAsync("doHover")<CR>
nmap <Leader><Leader>d <Plug>(coc-definition)
nmap <Leader><Leader>f <Plug>(coc-format)
nmap <Leader><Leader>t <Plug>(coc-type-definition)
nmap <Leader><Leader>r <Plug>(coc-rename)
nmap <Leader><Leader>e <Plug>(coc-refractor)
nmap <Leader><Leader>i <Plug>(coc-diagnostic-info)
nmap <Leader><Leader>a :CocAction<CR>
nmap <Leader><Leader>l <Plug>(coc-codelens-action)
nmap <Leader><Leader>m :MinimapToggle<CR>

" NerdTREE
nnoremap <Leader>t :NERDTreeToggle<CR>
autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Neoformat
nnoremap <Leader>f :Neoformat<CR>
let g:neoformat_enabled_python = ['autopep8']
let g:neoformat_python_autopep8 = {
            \ 'exe': 'autopep8-3'
            \ }
let g:neoformat_enabled_html = ['prettier']
let g:neoformat_enabled_lua = ['luafmt']
let g:neoformat_basic_format_align = 1
let g:neoformat_basic_format_retab = 1
let g:neoformat_basic_format_trim = 1

" CtrlP
let g:ctrlp_working_path_mode = 'a'
"let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_custom_ignore = {
            \ 'dir': 'target$'
            \ }
map <silent> <C-b> :CtrlPBuffer<cr>

" Table Mode
let g:table_mode_corner ='+'
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='
augroup mdtable
    autocmd!
    autocmd BufWritePre *.md let b:table_mode_corner ='+'
    autocmd BufWritePre *.md let b:table_mode_corner_corner='+'
    autocmd BufWritePre *.md let b:table_mode_header_fillchar='='
augroup END

" Statusline
let g:airline_theme='badwolf'
let g:bufferline_echo = 0

" Theme
set background=dark
colorscheme gruvbox
let g:gruvbox_contrast_dark='high'
nnoremap <Leader>bd :set background=dark<CR>
nnoremap <Leader>bl :set background=light<CR>

" Prosession
if has('win32')
    let g:prosession_dir='~/AppData/Local/nvim-data/prosession'
else
    let g:prosession_dir="~/.local/share/nvim/prosession"
end
let g:prosession_on_startup = 1

" Filetypes
"" Verilog
autocmd Filetype verilog_systemverilog setlocal makeprg=cd\ %:h\ &&\ pnmainc\ %:p:h/build.tcl\ \\\|\ grep\ -e\ @E\ -e\ @W
autocmd Filetype verilog_systemverilog setlocal errorformat=@%t:\ %*[A-Z]\%n\ :\"%f\":%l:%c:%*[0-9]:%*[0-9]\|%m
autocmd Filetype verilog_systemverilog setlocal errorformat+=@%t:\"%f\":%l:%c:%*[0-9]:%*[0-9]\|%m
autocmd Filetype verilog_systemverilog setlocal errorformat+=@%t:\ %m
autocmd Filetype verilog_systemverilog nnoremap <buffer> <F6> :make! <bar> vert copen<cr><C-w><C-=>
autocmd Filetype verilog_systemverilog setlocal noexpandtab
let g:verilog_disable_indent_lst = "module,package"
let g:verilog_syntax_fold_lst='all'

"" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"" LaTeX
autocmd Filetype tex setlocal tw=79
let g:vimtex_compiler_method = 'latexmk'
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'
let g:tex_flavor = 'latex'
let g:vimtex_fold_enabled = 1
"let g:vimtex_quickfix_latexlog = {'default': 0}
let g:vimtex_indent_on_ampersands = 0
let g:vimtex_delim_toggle_mod_list = [
            \ ['\left', '\right'],
            \ ['\big', '\big'],
            \ ['\Big', '\Big'],
            \ ['\bigg', '\bigg'],
            \ ['\Bigg', '\Bigg'],
            \]

"" Python
autocmd Filetype python nnoremap <buffer> <F6> :exec '!python' shellescape(@%, 1)<cr>
"augroup fmt
    "autocmd!
    "autocmd BufWritePre *.py undojoin | Neoformat
"augroup END

"" Rust
let g:rustfmt_autosave = 1

" Switch
let g:switch_maping = ''
map <silent> <C-A-x> :Switch<CR>

" NeoMake
let g:neomake_open_list = 2
nnoremap <buffer> <F5> :Neomake<CR>

" Undotree
nnoremap <Leader>\u :UndotreeToggle<CR>

" Templates
augroup templates
    autocmd BufNewFile p*.rs 0r ~/.config/nvim/templates/projecteuler.rs
augroup END
